
This is a Qt app that is basically a browser that runs on a Linux device with touchscreen.

Since it uses the Qt virtual keyboard under open source license. This code is also bound by that open source license.
http://doc.qt.io/qt-5/qtvirtualkeyboard-index.html#licenses-and-attributions

