import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Controls 1.0
import QtQuick.XmlListModel 2.0

Page {
    id: page2
    anchors.fill: parent
    Rectangle{
        anchors.fill: parent
        color: "#0381b2"
    }

        header: ToolBar{
        id: signSelectionToolbar
        height: 60
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0

        Button{
            id: backtoSign


            //text: qsTr("Back")
            iconSource: "file:///AMSI/User/QtRoadside/Images/112_LeftArrowLong_Blue_48x48_72.png"
            //iconSource: "file:///home/jeremy/112_LeftArrowLong_Blue_48x48_72.png"
            height: parent.height
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 1
            anchors.top: parent.top
            anchors.topMargin: 1
            anchors.left: parent.left
            anchors.leftMargin: 1
            onClicked: {page1.visible= true;
                        page2.visible = false
                        myTimer.restart()
                        }
    }
        Text {
            id: currentSign
            text: "Current Sign: " + signURL

           anchors.centerIn: parent

            }

    }

        XmlListModel {
            id: itemmodel
            source: "file:///AMSI/User/Roadside/signman.xml"
            //source: "file:///home/jeremy/signman.xml"
            query: "/SignManager/Signs/SignInfo"
            XmlRole {name: "ipAddress"; query: "IP/string()"}
            //XmlRole {name: "signName"; query: "SignName/string()"}

        }


MiscXmlListBuilder{}


             ListModel{
                 id: lv
                 }


    GridView {
        id: grid
        anchors.fill: parent
        cellWidth: width/2; cellHeight: height/5
        Label {
                anchors.fill: parent
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                visible: parent.count == 0
                text: qsTr("NO SIGNS CONNECTED!")
                font.bold: true
                font.pointSize: 24
            }

        model: lv

        delegate: SignSelectionDelegate{}

    }
    MouseArea{
        anchors.fill: parent
        onPressed: { mouse.accepted=false; myTimer.restart(); console.log("timer restart from page 2")}

    }

    Text {
        id: softwareVersion
        text: qsTr("Application Version: "+appVersion)
        anchors.bottomMargin: 1
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }




}

