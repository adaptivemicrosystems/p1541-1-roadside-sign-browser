TEMPLATE = app
TARGET = qt5_roadside_app
QT += qml quick webengine
SOURCES += \
    main.cpp
CONFIG += link_pkgconfig disable-desktop

static {
    QT += svg
    QTPLUGIN += qtvirtualkeyboardplugin
}

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /AMSI/User/QtRoadside/
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    demo.qrc

OTHER_FILES += \
    Container.qml \
    content/AutoScroller.qml \
    content/HandwritingModeButton.qml \
    content/ScrollBar.qml \
    content/TextArea.qml \
    content/TextBase.qml \
    content/TextField.qml \
    SignSelectionDelegate.qml \
    signman.xml \


disable-xcb {
    message("The disable-xcb option has been deprecated. Please use disable-desktop instead.")
    CONFIG += disable-desktop
}


    DEFINES += MAIN_QML=\\\"Container.qml\\\"

DISTFILES += \
    QtRoadsideApp.qml
