/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Qt Virtual Keyboard module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.7
//import QtQuick.VirtualKeyboard 2.1
import QtWebEngine 1.2
import QtQuick.Controls 2.0
import QtQuick.Controls 1.0
import QtQuick.XmlListModel 2.0
import QtQuick.Layouts 1.3
//import QtQml 2.2
import QtQuick.Controls.Styles 1.0

import "content"


Page{
    id:page
    width: 800
    height: 480
    property var appVersion: String("1.0.1")
    //property var signURL: String("http://10.1.110.60:1080")
    //property var originalSignURL: String("http://10.1.110.60:%10080")
    property var signURL: String("https://10.11.11.101:443")
    property var originalSignURL: String("https://10.11.11.")
    property int i: 1
    property int myIterator: 1
    //property int currentSignName: ""
    property int counter:0
    property variant statesNames: ["state1", "state2","state3", "state4","state5"]



    StackLayout {

        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.topMargin: 0


Page {
            id: page1

            anchors.top: parent.top
            anchors.topMargin: 0



            header: ToolBar{
                id: toolBarTop
                height: 60
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0


                Text {
                    id: webPageTitle
                    font.bold: true
                    font.pointSize: 10
                    anchors.centerIn: parent

                    }

                    Button{
                    id: goBack


                    //text: qsTr("Back")
                    iconSource: "file:///AMSI/User/QtRoadside/Images/112_LeftArrowLong_Blue_48x48_72.png"
                    //iconSource: "file:///home/jeremy/112_LeftArrowLong_Blue_48x48_72.png"

                    height: parent.height
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 1
                    anchors.top: parent.top
                    anchors.topMargin: 1
                    anchors.left: parent.left
                    anchors.leftMargin: 1
                    onClicked:{ wView.goBack()
                                myTimer.restart()
                               }
            }
               Button{

                id: homeButton
                iconSource: {"file:///AMSI/User/QtRoadside/Images/if_Home_131971.png"  }
                 //iconSource: "file:///home/jeremy/if_Home_131971.png"

                //text: qsTr("Home")

                anchors.bottom: parent.bottom
                anchors.bottomMargin: 1
                anchors.left: goBack.right
                anchors.leftMargin: 5
                anchors.top: parent.top
                anchors.topMargin: 1
                onClicked:{ wView.url = signURL +"/?hide_toolbar=true"
                            myTimer.restart()
                          }

                }


               Button {
                    id: logoutButton
                       anchors.right: parent.right
                    //text: qsTr("Logout")
                    iconSource: "file:///AMSI/User/QtRoadside/Images/112_ArrowCurve_Blue_Right_48x48_72.png"
                       //iconSource: "file:///home/jeremy/112_ArrowCurve_Blue_Right_48x48_72.png"
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 1
                    anchors.leftMargin: 5
                    anchors.top: parent.top
                    anchors.topMargin: 1
                    anchors.rightMargin: 1
                    onClicked: {wView.url = signURL +"/users/logout/?hide_toolbar=true"
                                myTimer.restart()
                                }
                    }

               Button {
                   id: ipSwitch2
                   visible: true

                   text: qsTr("Switch Sign")
                   anchors.bottomMargin: 1
                   anchors.bottom: parent.bottom
                   anchors.right: logoutButton.left
                   anchors.top: parent.top
                   anchors.rightMargin: 5
                   onClicked: {page1.visible= false; page2.visible = true; myTimer.restart();
                                wView.focus=false;


                   }

                   }


            }



            WebEngineView{
                id: wView
                anchors.fill: parent
                url: signURL +"/?hide_toolbar=true"

                onCertificateError: {
                                    error.ignoreCertificateError();

                }

                onTitleChanged:   webPageTitle.text = qsTr(wView.title)


            }


            MouseArea{

            anchors.fill: parent
            onPressed:  { mouse.accepted = false
                          myTimer.restart();
                         console.log("timer restart from page 1")}

        }


        }


SignSelectionPage{
id:page2
}
ScreenSaverPage{
id:blankPage
}
ScreenSaverTimer{
id:myTimer
}




    }

}

