import QtQuick 2.7
import QtQuick.Controls 2.0

Page{
    id: blankPage
    anchors.fill: parent


    MouseArea{
        anchors.fill: parent
        onClicked: { myTimer.restart(); page1.visible=true, page2.visible=false, blankPage.visible=false; console.log("timer restart from page 3")}

    }

    Rectangle {
      id:top1
     anchors.fill:parent
      color:"black"

      Image {
          id: userIcon

            source: "file:///AMSI/User/QtRoadside/Images/Adaptive_Logo.jpg"
            //source: "file:///home/jeremy/Adaptive_Logo.jpg"
      }



      states: [
        State {
          name: "state1"
          PropertyChanges {target:userIcon; x:1 ; y:1}
        },
          State {
          name: "state2"
          PropertyChanges {target:userIcon; x: page.width-userIcon.width; y:page.height-userIcon.height}
        },
          State {
            name: "state3"
            PropertyChanges {target:userIcon; x: 1; y:page.height-userIcon.height}
          },
          State {
            name: "state4"
            PropertyChanges {target:userIcon; x: page.width-userIcon.width; y:1}
          },
          State {
            name: "state5"
            PropertyChanges {target:userIcon; x: page.width/2; y:page.height/1.3}
          }
      ]

      transitions: [


          Transition {
              from: "*"; to: "*"
              NumberAnimation { properties: "x,y"; duration: 3000 }
          }


      ]

      Timer {
        id:zen
        interval: 3000;
        running: true;
        repeat: true

        onTriggered: {
          if (counter < 5) {
            top1.state = statesNames[counter]
            counter = counter + 1
          } else{
            counter = 1
            top1.state = statesNames[0]}
        }
      }
    }



}
