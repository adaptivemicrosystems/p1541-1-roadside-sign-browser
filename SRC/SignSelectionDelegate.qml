import QtQuick 2.7

import QtQuick.Controls 1.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4


Button{
        height: grid.cellHeight;
        width: grid.cellWidth;
        id: button;

        style: ButtonStyle {
               background: Rectangle {
                implicitWidth: 100
                implicitHeight: 25
                border.width: 3
                border.color: "#000"
                radius: 20
                gradient: Gradient {
                    GradientStop { position: 0 ; color: control.pressed ? "#ccc" : "#eee" }
                    GradientStop { position: 1 ; color: control.pressed ? "#aaa" : "#ccc" }
                }
            }
        }
                    Text{
                            anchors.centerIn: parent
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            text: signName+"\n"+ ip

                    }



                    MouseArea {
                                 anchors.fill: parent
                                 onClicked: {
                                 grid.currentIndex = index;


                                     if(signURL !== originalSignURL + (grid.currentIndex+101)+":443"){
                                          wView.url = signURL + "/users/logout/?hide_toolbar=true"
                                         myTimer.restart()

                                     }

                                            signURL = originalSignURL + (grid.currentIndex+101) +":443";
                                            wView.url = signURL + "/?hide_toolbar=true";
                                            console.log(signURL);
                                            page1.visible= true;
                                            page2.visible = false;


                                   }
                                }

                   }
