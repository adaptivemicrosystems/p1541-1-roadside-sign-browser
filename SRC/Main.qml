/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Qt Virtual Keyboard module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.7
import QtQuick.VirtualKeyboard 2.1
import QtWebEngine 1.2
import QtQuick.Controls 2.0
import QtQuick.Controls 1.0
import QtQuick.XmlListModel 2.0
import QtQuick.Layouts 1.3
import QtQml 2.2
import QtQuick.Controls.Styles 1.0
import "content"


Page{
    id: page
    width: 800
    height: 480
    property var appVersion: String("1.0.0")
    //property var signURL: String("http://10.1.110.60:1080")
    // property var originalSignURL: String("http://10.1.110.60:%10080")
    property var signURL: String("http://10.11.11.101")
    property var originalSignURL: String("http://10.11.11.")
    //property var signURL: String("https://10.11.11.101:443")
    property int i: 1
    property int myIterator: 1
    //property int currentSignName: ""
    property int counter:0
    property variant statesNames: ["state1", "state2","state3", "state4","state5"]


        Timer {
            id: myTimer
            interval: 300000; running: true; repeat: false
            onTriggered: { page1.visible=false;
                           page2.visible=false;
                           blankPage.visible=true;

        }

        }




   StackLayout {

        id: swipeView
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.topMargin: 0


        Page {
            id: page1

            anchors.top: parent.top
            anchors.topMargin: 0



            header: ToolBar{
                id: toolBarTop
                height: 60
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0


                Text {
                    id: webPageTitle
                    font.bold: true
                    font.pointSize: 10
                    anchors.centerIn: parent

                    }

                    Button{
                    id: goBack


                    //text: qsTr("Back")
                    iconSource: "file:///AMSI/User/QtRoadside/Images/112_LeftArrowLong_Blue_48x48_72.png"

                    height: parent.height
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 1
                    anchors.top: parent.top
                    anchors.topMargin: 1
                    anchors.left: parent.left
                    anchors.leftMargin: 1
                    onClicked:{ wView.goBack()
                                myTimer.restart()
                               }
            }
               Button{

                id: homeButton
                iconSource: {"file:///AMSI/User/QtRoadside/Images/if_Home_131971.png"  }

                //text: qsTr("Home")

                anchors.bottom: parent.bottom
                anchors.bottomMargin: 1
                anchors.left: goBack.right
                anchors.leftMargin: 5
                anchors.top: parent.top
                anchors.topMargin: 1
                onClicked:{ wView.url = signURL +"/?hide_toolbar=true"
                            myTimer.restart()
                          }

                }


               Button {
                    id: logoutButton
                       anchors.right: parent.right
                    //text: qsTr("Logout")
                    iconSource: "file:///AMSI/User/QtRoadside/Images/112_ArrowCurve_Blue_Right_48x48_72.png"
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 1
                    anchors.leftMargin: 5
                    anchors.top: parent.top
                    anchors.topMargin: 1
                    anchors.rightMargin: 1
                    onClicked: {wView.url = signURL +"/users/logout/?hide_toolbar=true"
                                myTimer.restart()
                                }
                    }

               Button {
                   id: ipSwitch2
                   visible: true

                   text: qsTr("Switch Sign")
                   anchors.bottomMargin: 1
                   anchors.bottom: parent.bottom
                   anchors.right: logoutButton.left
                   anchors.top: parent.top
                   anchors.rightMargin: 5
                   onClicked: {page1.visible= false; page2.visible = true; myTimer.restart();

                   }

                   }





            }



            WebEngineView{
                id: wView
                anchors.fill: parent
                url: signURL +"/?hide_toolbar=true"

                onCertificateError: {
                                    error.ignoreCertificateError();

                }

                onTitleChanged:   webPageTitle.text = qsTr(wView.title)


            }

            MouseArea{

            anchors.fill: parent
            onPressed:  { mouse.accepted = false
                          myTimer.restart();
                         console.log("timer restart from page 1")}

        }








       }

        Page {
            id: page2
            anchors.fill: parent
            Rectangle{
                anchors.fill: parent
                color: "#0381b2"
            }

                header: ToolBar{
                id: signSelectionToolbar
                height: 60
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0

                Button{
                    id: backtoSign


                    //text: qsTr("Back")
                    iconSource: "file:///AMSI/User/QtRoadside/Images/112_LeftArrowLong_Blue_48x48_72.png"
                    height: parent.height
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 1
                    anchors.top: parent.top
                    anchors.topMargin: 1
                    anchors.left: parent.left
                    anchors.leftMargin: 1
                    onClicked: {page1.visible= true;
                                page2.visible = false
                                myTimer.restart()
                                }
            }
                Text {
                    id: currentSign
                    text: "Current Sign: " + signURL

                   anchors.centerIn: parent

                    }

            }

                XmlListModel {
                    id: itemmodel
                    source: "file:///AMSI/User/Roadside/signman.xml"
                    query: "/SignManager/Signs/SignInfo"
                    XmlRole {name: "ipAddress"; query: "IP/string()"}
                    //XmlRole {name: "signName"; query: "SignName/string()"}

                }


            Instantiator {
                id: instantiator
                model: itemmodel.count+1
                delegate: XmlListModel{
                    id: mode
                    source: "http://10.11.11."+(modelData+101)+"/NTCIP/misc.xml"
                    query: "/MiscInfo"
                    XmlRole {name: "ipAddress"; query: "sysName/string()"}

                    onStatusChanged:{
                                if (status === XmlListModel.Loading) {
                                    //console.log("Loading..." + "-"+source)
                                }
                                if (status === XmlListModel.Ready) {
                                    console.log("Loaded " + source)
                                    console.log(mode.get(0).ipAddress)
                                    //console.log(instantiator.count)
                                    lv.append({signName: mode.get(0).ipAddress, ip: modelData+1})
                                }
                                if (status === XmlListModel.Error) {
                                        console.log("Xml Error: " + errorString())
                                          console.log(mode.source)
                                }
                            }

                }

        }


                     ListModel{
                         id: lv

                         }


            GridView {
                id: grid
                anchors.fill: parent
                cellWidth: width/2; cellHeight: height/5
                Label {
                        anchors.fill: parent
                        horizontalAlignment: Qt.AlignHCenter
                        verticalAlignment: Qt.AlignVCenter
                        visible: parent.count == 0
                        text: qsTr("NO SIGNS CONNECTED!")
                        font.bold: true
                        font.pointSize: 24
                    }

                model: lv

                delegate: SignSelectionDelegate{}

            }
            MouseArea{
                anchors.fill: parent
                onPressed: { mouse.accepted=false; myTimer.restart(); console.log("timer restart from page 2")}

            }

            Text {
                id: softwareVersion
                text: qsTr("Application Version: "+appVersion)
                anchors.bottomMargin: 1
                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
            }




  }
        Page{
            id: blankPage
            anchors.fill: parent

           /* Rectangle{
                anchors.fill: parent
                color: "black"
            }

            Rectangle {
                width: animation.width; height: animation.height + 8
                anchors.centerIn:  parent
                color: "black"
                AnimatedImage { id: animation; source: "file:///AMSI/User/QtRoadside/Images/adaptive_logo.jpg" }


            }*/

            MouseArea{
                anchors.fill: parent
                onClicked: { myTimer.restart(); page1.visible=true, page2.visible=false, blankPage.visible=false; console.log("timer restart from page 3")}

            }

            Rectangle {
              id:top1
             anchors.fill:parent
              color:"black"

              Image {
                  id: userIcon

                    source: "file:///AMSI/User/QtRoadside/Images/Adaptive_Logo.jpg"
              }



              states: [
                State {
                  name: "state1"
                  PropertyChanges {target:userIcon; x:1 ; y:1}
                },
                  State {
                  name: "state2"
                  PropertyChanges {target:userIcon; x: page.width-userIcon.width; y:page.height-userIcon.height}
                },
                  State {
                    name: "state3"
                    PropertyChanges {target:userIcon; x: 1; y:page.height-userIcon.height}
                  },
                  State {
                    name: "state4"
                    PropertyChanges {target:userIcon; x: page.width-userIcon.width; y:1}
                  },
                  State {
                    name: "state5"
                    PropertyChanges {target:userIcon; x: page.width/2; y:page.height/2}
                  }
              ]

              transitions: [


                  Transition {
                      from: "*"; to: "*"
                      NumberAnimation { properties: "x,y"; duration: 3000 }
                  }


              ]

              Timer {
                id:zen
                interval: 3000;
                running: true;
                repeat: true

                onTriggered: {
                  if (counter < 5) {
                    top1.state = statesNames[counter]
                    counter = counter + 1
                  } else{
                    counter = 1
                    top1.state = statesNames[0]}
                }
              }
            }



        }




    }

}

