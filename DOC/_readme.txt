===== REPO =====

\\commserver\ReleaseVSS\REPOS\ITS\P1541-1 Roadside Sign Browser.git

===== DESCRIPTION =====

This is an application that runs on the AX1550+ Roadside (aka Roadside with touchscreen).
It provides a means to navigate the webpages of attached ITS signs loaded with webpages.
The AMS PN for this application is 1541660002.

===== EXTERNAL DOCS OR PROJECT FOLDERS =====

J:\ITS Projects\P1541-1 ADDITION OF LCD KEYBOARD INTERFACE (HMI)

===== EXTERNAL CODE PROJECTS =====

This is the NTCIP webpage typically served. However the app should work with any page on standard port numbers.
\\commserver\ReleaseVSS\REPOS\ITS\P1541-1 NTCIP Sign Webpage.git

===== TOOLS =====

The app is based on Qt 5.8 and built with the nitrogen6x SDK (presently yocto-pyro).
Qt creator was used with cross compiling in a linux environment.

===== BUILD =====

Indicate any part numbers that might be produce through this package and how to build them.
Indicate build instructions such as any DEFINEs that may need setting, build scripts, file shuffling, or absolute path requirements.

===== DEPLOYMENT =====

Indicate what this code produces. What is the deliverable?
Indicate how to build an installer if it has one.
Indicate any special instruction on what to do with the deliverable.

===== ECO NOTES =====

List any special items that would need consideration in a release ECO.
(Such as labels that need changing or test databases needing revision field updates?)

===== VERSIONING =====

There is a "@(#)" string line in the code that indicates PN and revision of the build.
